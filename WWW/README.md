# Docker for local web development with PHP, MySQL, Nginx

99.99% based on the tutorial series ["Docker for local web development, introduction: why should you care?"](https://tech.osteel.me/posts/docker-for-local-web-development-introduction-why-should-you-care), by Yannick Chenot.

## What it does
Sets up a basic development environment for PHP, with optional MySQL, PHPmyAdmin, and Mailhog. It sets up a new domain in hosts file, SSL certificates for the domain, and uses an NGINX proxy server for HTTP2 traffic.

## Content

This branch contains a stack running on Docker, which includes:

* An alpine container for Nginx;
* An alpine container for PHP-FPM;
* A container for MySQL;
* An alpine container for phpMyAdmin;
* A volume for MySQL data;
* Mailhog for email testing.

The containers are based on Alpine images when available, for an optimised size (However, I wonder if in the long run, full versions are most space-saving effective, due to the caching of images when building the containers...) 

## Pre-requisites

Make sure [Docker Desktop for Mac or PC](https://www.docker.com/products/docker-desktop) is installed and running, or head [over here](https://docs.docker.com/install/) if you are a Linux user. You will also need a terminal running [Git](https://git-scm.com/).

This setup also uses localhost's port 80, so make sure it is available (for instnace, kill any mamp installation that you have running).

## Directions

Everything can be controlled from the 'super' bash file. To initialize a new environment:

* update `.env.example`
* Run the initializing command: `$ bash super init` , which will create the containers, create domain in hosts file, and create certificates

## Issues

Sometimes I have had to restart the containers to make them work try:

* `$ bash super recreate`, which basically deletes the containers and rebuilds them again


## Results

### Application

The PHP application will be available at https://yourdomain.local

The `src/` directory containing the application is mounted onto both Nginx's and the application's containers, meaning any update to the code is immediately available upon refreshing the page, without having to rebuild any container.

### PHP My Admin

And PHP My Admin will be found at [phpmyadmin.local](https://phpmyadmin.local)

### Database

The database data is persisted in its own directory through the volume `./mysqldata`.

### MailHog

Mailhog interface will be available at [mailHog](http://localhost:8025/)


## Slimmer set up
If a service is not needed, for instance MySQL:
- Update `docker-compose.yml`: Comment out the MySQL and PHPMyAdmin services and other lines of code in the file that refer to them;
- Comment out all the lines in `.docker/nginx/conf.d/phpmyadmin.conf`.


## Useful Docker Commands

The following commands are for reference only. All of this actions can be executed from the `super` bash file. 

To stop the containers:

```
$ docker-compose stop
```

To destroy the containers:

```
$ docker-compose down
```

To destroy the containers and the associated volumes:

```
$ docker-compose down -v
```

To remove everything, including the images:

```
$ docker-compose down -v --rmi all
```

To force the rebuilding of the containers

```
docker-compose up -d --force-recreate  --build
```