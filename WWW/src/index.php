<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Hello there</title>
        <style>
            body {
                font-family: "Arial", sans-serif;
                font-size: larger;
            }

            .center {
                display: block;
                margin-left: auto;
                margin-right: auto;
                width: 50%;
            }
        </style>
    </head>
    <body>
        <img src="https://tech.osteel.me/images/2020/03/04/hello.gif" alt="Hello there" class="center">
   
        <h2>MAILHOG TEST</h2>    
        <p>check mail hog: <a href="http://localhost:8025" target="_blank">Check Mail</a></p>    
        <?php  
            # sends test email
            $headers = 'From: mailhog@mailhog.com' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();    
            mail ( 'contact@mauricioarango.net' , 'Test' , 'Your docker is up and running.', $headers ); 
        ?>
        <br>
        <br>
        <h2>Database Test</h2>
        <?php
        $connection = new PDO('mysql:host=mysql;dbname=standalone;charset=utf8', 'root', 'password');
        $query      = $connection->query("SELECT TABLE_NAME FROM information_schema.TABLES WHERE TABLE_SCHEMA = 'demo'");
        $tables     = $query->fetchAll(PDO::FETCH_COLUMN);

        if (empty($tables)) {
            echo '<p class="center">There are no tables in database <code>demo</code>.</p>';
        } else {
            echo '<p class="center">Database <code>demo</code> contains the following tables:</p>';
            echo '<ul class="center">';
            foreach ($tables as $table) {
                echo "<li>{$table}</li>";
            }
            echo '</ul>';
        }
        ?>
    </body>
</html>