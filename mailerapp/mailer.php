<?php
#  PROCESS THE INFORMATION AND SENDS EMAILS...

$emailQuote = 'hi@mauricioarango.net';
$emailSample = 'hellolittleone@gmail.com';
$quote = '';
$sample = '';

# Determines what emails, quote &/or sample, need to be sent:

# Creates list of products for quote aand for sample requests

function processProducts ($input){
   global $quote, $sample;
   $products = $input['products'];

    foreach ($products as $product):

        if ($product['needs'][0]['sample'] == 'true'):

            $sample .= productFormatter($product);
            $sample .= '</p>'.'<p>&nbsp;</p>'; #only needs closing p. the opener was added in product Formatter
        endif;

        if ($product['needs'][1]['quote'] == 'true'):
            $quote .= productFormatter($product);
            $quote .= 'Dimensions: '. $product['needs'][2]['dimensions'];
            $quote .= '</p>'.'<p>&nbsp;</p>';
        endif;

    endforeach;



}

function productFormatter ($product){
    $string = '<h3><a href="'.filter_var($product['url'], FILTER_SANITIZE_URL).'">';
    $string .=  '<strong>'.filter_var($product['name'], FILTER_SANITIZE_STRING).'</strong>';
    $string .= '</a></h3>';
    $string .= '<p>';
    $colors = implode( ', ', $product['color'] );
    $string .= '<strong>'.'Colors: '.'</strong>'.filter_var($colors, FILTER_SANITIZE_STRING)."<br>";
    $fibers = implode( ', ', $product['fiber'] );
    $string .= '<strong>'.'Fibers: '.'</strong>'.filter_var($fibers, FILTER_SANITIZE_STRING)."<br>";
    $string .= '<strong>'.'Mill #: '.'</strong>'.filter_var($product['vendor'], FILTER_SANITIZE_STRING)."<br>";
    $string .= '<strong>'.'SKU: '.'</strong>'.filter_var($product['sku'], FILTER_SANITIZE_STRING)."<br>";
    if (isset($product['note'])){
        $string .= 'Notes: '.filter_var($product['note'], FILTER_SANITIZE_STRING)."<br>";
    }

    return $string;
}

function customerInfo ($input){
    $customer = '';
    $fname = filter_var( $input['fname'], FILTER_SANITIZE_STRING );
    $lname = filter_var( $input['lname'], FILTER_SANITIZE_STRING );
    $email = filter_var( $input['email'], FILTER_SANITIZE_EMAIL );
    $telephone = filter_var( $input['telephone'], FILTER_SANITIZE_STRING );
    $company = filter_var( $input['company'], FILTER_SANITIZE_STRING );
    $address1 = filter_var( $input['address1'], FILTER_SANITIZE_STRING );
    $address2 = filter_var( $input['address2'], FILTER_SANITIZE_STRING );
    $city = filter_var( $input['city'], FILTER_SANITIZE_STRING );
    $region = filter_var( $input['region'], FILTER_SANITIZE_STRING );
    $postal = filter_var( $input['postal'], FILTER_SANITIZE_STRING );
    $country = filter_var( $input['country'], FILTER_SANITIZE_STRING );
    $telephone = filter_var( $input['telephone'], FILTER_SANITIZE_STRING );


    $customer = "<h2><strong>From: </strong>".$fname." ".$lname."</h2>";
    $customer .= '<p>';
    $customer .= "<strong>Email: </strong>"."<a href='mailto:".$email."'>".$email."</a><br>";
    $customer .= "<strong>Telephone: </strong>".$telephone."<br>";
    if (!empty($company)) $customer .= "<strong>Company: </strong>".$company."<br>";
    $customer .= "<strong>Address: </strong>"."<br>";
    $customer .= $address1."<br>";
    if (!empty($address2)) $customer .= $address2."<br>";
    $customer .= $city.", ".$region." ".$postal."<br>";
    $customer .= $country;
    $customer .= '</p>';

    return "$customer".'<p>&nbsp;</p><hr><p>&nbsp;</p>';


}

function mail_sendEmails($input){
    // creates the sample and quote product strings
    processProducts($input);

    global $quote, $sample, $emailQuote, $emailSample;

    // Emails headers
    $headers = "From: ".$input['fname']." ".$input['lname']." <".$input['email'].">\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n"; #so tha hyperlinks show up as such and not plain text

    # Mail Content
    $opener = '<html><body>';
    $closer = '</body></html>';
    $customer = customerInfo($input);


    # sends the emails
    if (!empty($quote)){
        $subject ='Quote Request from Broadloom.com';
        $message ='<h1>New Quote Request</h1>';
        $body = $opener.$message.$customer.$quote.$closer;
        $body = wordwrap($body, 70, "\r\n");
        $successQuote = mail($emailQuote, $subject, $body, $headers);
    }else{
        $successQuote = true; #makes it true, so that there's no error to report
    }
    if (!empty ($sample)){
        $subject ='Sample Request from Broadloom.com';
        $message ='<h1>New Sample Request</h1>';
        $body = $opener.$message.$customer.$sample.$closer;
        $body = wordwrap($body, 70, "\r\n");
        $successSample = mail($emailSample, $subject, $body, $headers);
    }else{
        $successSample = true;
    }

    if ( $successQuote && $successSample ){
        return true;
    }else{
        return false;
    }


}
?>