<?php
# In order to enable CORS set these headers:
header("Access-Control-Allow-Origin: *");
header('Content-Type: application/json');
header ("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
#
require('validation.php');
require('mailer.php');

// converts the received json into an array
$input = json_decode(file_get_contents('php://input'), true);
// findsout if there's any missing information
$errors = val_validateInput($input);


// Sends form
if ( empty($errors) ):
    // sends emails
    $process = mail_sendEmails($input);
    if ($process){
        echo json_encode([
                    "process" => "1"
                ]);
    }else{
        echo json_encode([
                    "process" => "0",
                    "error" => ["There was an error processing the email."]
                ]);
    }


else:
    // sends error message
    echo json_encode([
        "process" => "0",
        "error" => $errors
    ]);
endif;

?>