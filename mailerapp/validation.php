<?php
# VALIDATES FORM INPUT DATA

function val_validateInput ( $input ){
$validationErrors = [];
    // Makes sure product info was sent
    if ( isset($input['products']) && !empty($input['products']) ):
        $products = $input['products'];
    else:
        $validationErrors[] = 'Products were not added to this request.';
    endif;

    // Makes sure full contact info was sent
    $contactInfo = [
        "First Name" => $input['fname'],
         "Last Name" => $input['lname'],
         "Email" => $input['email'],
         "Telephone" => $input['telephone'],
         "Address Line 1" => $input['address1'],
         "City" => $input['city'],
         "Region or State" => $input['region'],
         "Postal code" => $input['postal'],
         "Country" => $input['country'],
    ];

    foreach ( $contactInfo as $key => $value ):
        if ( empty($value) ) $validationErrors[] = 'Missing '.$key;
    endforeach;

    // Validates Email
    if (!filter_var($input['email'], FILTER_VALIDATE_EMAIL)) $validationErrors[] = "Invalid email format.";

    return $validationErrors;
}


?>